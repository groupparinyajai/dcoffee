/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.groupparinyajai.dao;

import com.mycompany.groupparinyajai.helper.DatabaseHelper;
import com.mycompany.groupparinyajai.model.CheckInOut;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mookda
 */
public class CheckInOutDao implements Dao<CheckInOut> {

    @Override
    public CheckInOut get(int id) {
        CheckInOut checkInOut = null;
        String sql = "SELECT * FROM check_in_out WHERE cio_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                checkInOut = CheckInOut.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkInOut;
    }

    public List<CheckInOut> getAll() {
        ArrayList<CheckInOut> list = new ArrayList();
        String sql = "SELECT * FROM check_in_out";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInOut checkInOut = CheckInOut.fromRS(rs);
                list.add(checkInOut);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CheckInOut> getAllByUserId(int userId) {
        ArrayList<CheckInOut> list = new ArrayList();
        String sql = "SELECT * FROM check_in_out WHERE emp_id = ? ORDER BY cio_id asc";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, userId);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                CheckInOut checkInOut = CheckInOut.fromRS(rs);
                list.add(checkInOut);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<CheckInOut> getAll(String where, String order) {
        ArrayList<CheckInOut> list = new ArrayList();
        String sql = "SELECT * FROM check_in_out where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInOut checkInOut = CheckInOut.fromRS(rs);
                list.add(checkInOut);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CheckInOut> getAll(String order) {
        ArrayList<CheckInOut> list = new ArrayList();
        String sql = "SELECT * FROM check_in_out  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInOut checkInOut = CheckInOut.fromRS(rs);
                list.add(checkInOut);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckInOut save(CheckInOut obj) {
//check user_
        String sql = "INSERT INTO check_in_out (cio_total_hour , emp_id, ss_id)"
                + "VALUES( ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotalHour());
            stmt.setInt(2, obj.getUserId());
            stmt.setInt(3, obj.getSalaryId());

//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckInOut update(CheckInOut obj) {
        String sql = "UPDATE check_in_out"
                + " SET cio_time_out = ?, cio_total_hour = ? , ss_id = ?"
                + " WHERE cio_id = ? AND cio_date = ? AND emp_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        LocalTime currentTime = LocalTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        String formattedTime = currentTime.format(formatter);

        LocalTime localTime1 = LocalTime.parse(obj.getTimeIn(), formatter);
        LocalTime localTime2 = LocalTime.parse(formattedTime, formatter);

        long hoursDifference = ChronoUnit.HOURS.between(localTime1, localTime2);

        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, formattedTime);
            stmt.setFloat(2, hoursDifference);
            stmt.setInt(3, obj.getSalaryId());
            stmt.setInt(4, obj.getId());
            stmt.setString(5, obj.getDate());
            stmt.setInt(6, obj.getUserId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
//            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckInOut obj) {
        String sql = "DELETE FROM check_in_out WHERE cio_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
