/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.groupparinyajai.dao;

import com.mycompany.groupparinyajai.helper.DatabaseHelper;
import com.mycompany.groupparinyajai.model.SummarySalary;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author chana
 */
public class SummarySalaryDao implements Dao<SummarySalary> {

    @Override
    public SummarySalary get(int id) {
        SummarySalary summarySalary = null;
        String sql = "SELECT * FROM summary_salary WHERE ss_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                summarySalary = SummarySalary.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return summarySalary;
    }

    public List<SummarySalary> getAll() {
        ArrayList<SummarySalary> list = new ArrayList();
        String sql = "SELECT * FROM summary_salary";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                SummarySalary summarySalary = SummarySalary.fromRS(rs);
                list.add(summarySalary);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<SummarySalary> getAll(String where, String order) {
        ArrayList<SummarySalary> list = new ArrayList();
        String sql = "SELECT * FROM summary_salary where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                SummarySalary summarySalary = SummarySalary.fromRS(rs);
                list.add(summarySalary);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<SummarySalary> getAll(String order) {
        ArrayList<SummarySalary> list = new ArrayList();
        String sql = "SELECT * FROM summary_salary  ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                SummarySalary summarySalary = SummarySalary.fromRS(rs);
                list.add(summarySalary);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public SummarySalary save(SummarySalary obj) {

        String sql = "INSERT INTO summary_salary (ss_work_hour,ss_salary, epm_id,)"
                + "VALUES(?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setString(1, obj.getWorkHour());
            stmt.setFloat(2, obj.getSalary());
            stmt.setInt(3, obj.getEmpId());
            //            System.out.println(stmt);
            stmt
                    .executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public SummarySalary update(SummarySalary obj) {
        String sql = "UPDATE summary_salary"
                + " SET ss_work_hour = ?,  ss_salary = ?,epm_id=?"
                + " WHERE ss_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setString(1, obj.getWorkHour());
            stmt.setFloat(2, obj.getSalary());
            stmt.setInt(3, obj.getEmpId());
            stmt.setInt(4, obj.getId());
//           System.out.println(stmt);

            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(SummarySalary obj) {
        String sql = "DELETE FROM summary_salary WHERE ss_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
}
