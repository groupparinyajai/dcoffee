/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.groupparinyajai.model;

import com.mycompany.groupparinyajai.dao.CheckMaterialDetailDao;
import com.mycompany.groupparinyajai.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kitta
 */
public class CheckMaterial {

    private int id;
    private Date createdDate;
    private int userId;
    private User user;
    private ArrayList<CheckMaterialDetail> cmd = new ArrayList();

    public CheckMaterial(int id, Date createdDate, int userId) {
        this.id = id;
        this.createdDate = createdDate;
        this.userId = userId;
    }

    public CheckMaterial(Date createdDate, int userId) {
        this.id = -1;
        this.createdDate = createdDate;
        this.userId = userId;
    }

    public CheckMaterial() {
        this.id = -1;
        this.createdDate = null;
        this.userId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ArrayList<CheckMaterialDetail> getCmd() {
        return cmd;
    }
  

    public void setCmd(ArrayList<CheckMaterialDetail> cmd) {
        this.cmd = cmd;
    }

    @Override
    public String toString() {
        return "CheckMaterial{" + "id=" + id  +", createdDate=" + createdDate  + ", userId=" + userId + ", user=" + user + '}';
    }

    public void addCheckMatDetail(CheckMaterialDetail cmd) {
        this.cmd.add(cmd);
    }

    public static CheckMaterial fromRS(ResultSet rs) {
        CheckMaterial check_material = new CheckMaterial();
        try {
            check_material.setId(rs.getInt("check_mat_id"));
            check_material.setCreatedDate(rs.getTimestamp("check_mat_date"));
            check_material.setUserId(rs.getInt("user_id"));
            
            CheckMaterialDetailDao cmdDao = new CheckMaterialDetailDao();
            check_material.setCmd((ArrayList<CheckMaterialDetail>) cmdDao.getByCheckMatId(check_material.getId()));
            
            UserDao userDao = new UserDao();
            User user = userDao.get(check_material.getUserId());
            check_material.setUser(user);
            
        } catch (SQLException ex) {
            Logger.getLogger(CheckMaterial.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return check_material;
    }

}
