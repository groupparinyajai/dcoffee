/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.groupparinyajai.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kitta
 */
public class BillDetail {
    private int id;
    private String name;
    private int amount;
    private float price;
    private float total;
    private int billId;
    private int materialId;

    public BillDetail(int id, String name, int amount, float price, float total, int billId, int materialId) {
        this.id = id;
        this.name = name;
        this.amount = amount;
        this.price = price;
        this.total = total;
        this.billId = billId;
        this.materialId = materialId;
    }

    public BillDetail(String name, int amount, float price, float total, int billId, int materialId) {
        this.id = -1;
        this.name = name;
        this.amount = amount;
        this.price = price;
        this.total = total;
        this.billId = billId;
        this.materialId = materialId;
    }
    
    
    public BillDetail() {
        this.id = -1;
        this.name = "";
        this.amount = 0;
        this.price = 0;
        this.total = 0;
        this.billId = 0;
        this.materialId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public int getBillId() {
        return billId;
    }

    public void setBillId(int billId) {
        this.billId = billId;
    }

    public int getMaterialId() {
        return materialId;
    }

    public void setMaterialId(int materialId) {
        this.materialId = materialId;
    }

    @Override
    public String toString() {
        return "BillDetail{" + "id=" + id + ", name=" + name + ", amount=" + amount + ", price=" + price + ", total=" + total + ", billId=" + billId + ", materialId=" + materialId + '}';
    }
    
    
    

    public static BillDetail fromRS(ResultSet rs) {
        BillDetail billDetail = new BillDetail();
        try {
            billDetail.setId(rs.getInt("bill_detail_id"));
            billDetail.setName(rs.getString("bill_detail_name"));
            billDetail.setAmount(rs.getInt("bill_detail_amount"));
            billDetail.setPrice(rs.getFloat("bill_detail_price"));
            billDetail.setTotal(rs.getFloat("bill_detail_total"));
            billDetail.setBillId(rs.getInt("bill_id"));
            billDetail.setMaterialId(rs.getInt("mat_id"));
        } catch (SQLException ex) {
            Logger.getLogger(BillDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return billDetail;
    }
    
    
}
