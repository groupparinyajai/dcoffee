/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.groupparinyajai.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author win10
 */
public class Employee {
     private int id;
    private String name;
    private String address;
    private String tel;
    private String email;
    private String position;
    private int hourlyWage;
    private int employeeId;

    public Employee(int id, String name, String address, String tel, String email, String position, int hourlyWage, int employeeId) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.tel = tel;
        this.email = email;
        this.position = position;
        this.hourlyWage = hourlyWage;
        this.employeeId = employeeId;
    }
    public Employee( String name, String address, String tel, String email, String position, int hourlyWage, int employeeId) {
        this.id = -1;
        this.name = name;
        this.address = address;
        this.tel = tel;
        this.email = email;
        this.position = position;
        this.hourlyWage = hourlyWage;
        this.employeeId = employeeId;
    }
    public Employee() {
        this.id = -1;
        this.name = "";
        this.address = "";
        this.tel = "";
        this.email = "";
        this.position = "";
        this.hourlyWage = 0;
        this.employeeId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPosition() {
        return position;
    }
    

    public void setPosition(String position) {
        this.position = position;
    }

    public int getHourlyWage() {
        return hourlyWage;
    }

    public void setHourlyWage(int hourlyWage) {
        this.hourlyWage = hourlyWage;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", address=" + address + ", tel=" + tel + ", email=" + email + ", position=" + position + ", hourlyWage=" + hourlyWage + ", employeeId=" + employeeId + '}';
    }
        
     public static Employee fromRS(ResultSet rs) {
        Employee employee = new Employee();
        try {
            employee.setId(rs.getInt("emp_id"));
            employee.setName(rs.getString("emp_name"));
            employee.setAddress(rs.getString("emp_address"));
            employee.setTel(rs.getString("emp_tel"));
            employee.setEmail(rs.getString("emp_email"));
            employee.setPosition(rs.getString("emp_position"));
            employee.setHourlyWage(rs.getInt("emp_hourly_wage"));
            employee.setEmployeeId(rs.getInt("user_id"));

        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return employee;
    }
      
}
