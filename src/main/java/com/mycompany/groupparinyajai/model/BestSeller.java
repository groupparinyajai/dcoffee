/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.groupparinyajai.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author win10
 */
public class BestSeller {
    private int id;
    private String name;
    private int totalamout;
    private float totalPrice;

    public BestSeller(int id, String name, int totalamout, float totalPrice) {
        this.id = id;
        this.name = name;
        this.totalamout = totalamout;
        this.totalPrice = totalPrice;
    }
    
       public BestSeller( String name, int totalamout, float totalPrice) {
        this.id = -1;
        this.name = name;
        this.totalamout = totalamout;
        this.totalPrice = totalPrice;
    }
          public BestSeller( ) {
        this.id = -1;
        this.name = "";
        this.totalamout = 0;
        this.totalPrice = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotalamout() {
        return totalamout;
    }

    public void setTotalamout(int totalamout) {
        this.totalamout = totalamout;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "BestSeller{" + "id=" + id + ", name=" + name + ", totalamout=" + totalamout + ", totalPrice=" + totalPrice + '}';
    }
           public static BestSeller fromRS(ResultSet rs) {
        BestSeller obj = new BestSeller();
        try {
            obj.setName(rs.getString("rcd_name"));
            obj.setTotalamout(rs.getInt("total_amount"));
            obj.setTotalPrice(rs.getFloat("total_price"));
        } catch (SQLException ex) {
            Logger.getLogger(BestSeller.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
           }
}
