/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.groupparinyajai.model;

import com.mycompany.groupparinyajai.dao.CustomerDao;
import com.mycompany.groupparinyajai.dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class Reciept {
    private int id;
    private int queue;
    private Date createdDate;
    private float discount;
    private float total;
    private float cash;
    private float change;
    private String payment;
    private int empId;
    private int customerId;
    private Employee emp;
    private Customer customer;
    private ArrayList<RecieptDetail> recieptDetails = new ArrayList<RecieptDetail>();
    private int totalQty;

    public Reciept(int id, int queue, Date createdDate, float discount, float total, float cash, float change, String payment, int empId, int customerId) {
        this.id = id;
        this.queue = queue;
        this.createdDate = createdDate;
        this.discount = discount;
        this.total = total;
        this.cash = cash;
        this.change = change;
        this.payment = payment;
        this.empId = empId;
        this.customerId = customerId;
    }

    public Reciept(int queue, Date createdDate, float discount, float total, float cash, float change, String payment, int empId, int customerId) {
        this.id = -1;
        this.queue = queue;
        this.createdDate = createdDate;
        this.discount = discount;
        this.total = total;
        this.cash = cash;
        this.change = change;
        this.payment = payment;
        this.empId = empId;
        this.customerId = customerId;
    }

    public Reciept(int queue, float discount, float total, float cash, float change, String payment,  int empId, int customerId) {
        this.id = -1;
        this.queue = queue;
        this.createdDate = null;
        this.discount = discount;
        this.total = total;
        this.cash = cash;
        this.change = change;
        this.payment = payment;
        this.empId = empId;
        this.customerId = customerId;
    }

    public Reciept(float cash,  int empId, int customerId) {
        this.id = -1;
        this.queue = 0;
        this.createdDate = null;
        this.discount = 0;
        this.total = 0;
        this.cash = cash;
        this.change = 0;
        this.payment = "";
        this.empId = empId;
        this.customerId = customerId;
    }

    public Reciept() {
        this.id = -1;
        this.queue = 0;
        this.createdDate = null;
        this.discount = 0;
        this.total = 0;
        this.cash = 0;
        this.change = 0;
        this.payment = "";
        this.empId = 0;
        this.customerId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQueue() {
        return queue;
    }

    public void setQueue(int queue) {
        this.queue = queue;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getCash() {
        return cash;
    }

    public void setCash(float cash) {
        this.cash = cash;
    }
    
    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public float getChange() {
        return change;
    }

    public void setChange(float change) {
        this.change = change;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public int getEmployeeId() {
        return empId;
    }

    public void setEmployeeId(int emp_Id) {
        this.empId = emp_Id;
    }
    
    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public Employee getEmployee() {
        return emp;
    }

    public void setEmployee(Employee emp) {
        this.emp = emp;
        this.empId = emp.getId();
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
        this.customerId = customer.getId();
    }
    
    public ArrayList<RecieptDetail> getRecieptDetails() {
        return recieptDetails;
    }

    public void setRecieptDetails(ArrayList recieptDetails) {
        this.recieptDetails = recieptDetails;
    }

    @Override
    public String toString() {
        return "Reciept{" + "id=" + id + ", queue=" + queue + ", createdDate=" + createdDate + ", discount=" + discount + ", total=" + total + ", cash=" + cash + ", change=" + change + ", payment=" + payment + ", employeeId=" + empId + ", customerId=" + customerId + ", emp=" + emp + ", customer=" + customer + ", recieptDetails=" + recieptDetails + '}';
    }
    
    public void addRecieptDetail(RecieptDetail recieptDetail) {
        recieptDetails.add(recieptDetail);
        calculateTotal();
    }
    
    public void addRecieptDetail(Product product, int qty) {
        RecieptDetail rd = new RecieptDetail(product.getId(), product.getName(), 
                product.getPrice(), qty, product.getPrice(), -1);
        recieptDetails.add(rd);
        calculateTotal();
    }

    public void delRecieptDetail(RecieptDetail recieptDetail) {
        recieptDetails.remove(recieptDetail);
        calculateTotal();
    }

    public void calculateTotal() {
        int totalQty = 0;
        float total = 0.0f;
        for (RecieptDetail rd : recieptDetails) {
            total += rd.getTotalPrice();
            totalQty += rd.getQty();
        }
        this.totalQty = totalQty;
        this.total = total;
    }
    
    public static Reciept fromRS(ResultSet rs) {
        Reciept reciept = new Reciept();
        try {
            reciept.setId(rs.getInt("rec_id"));
            reciept.setQueue(rs.getInt("rec_queue"));
            reciept.setCreatedDate(rs.getTimestamp("rec_date"));
            reciept.setDiscount(rs.getFloat("rec_discount"));
            reciept.setTotal(rs.getFloat("rec_total"));
            reciept.setCash(rs.getFloat("rec_cash"));
            reciept.setChange(rs.getFloat("rec_change"));
            reciept.setPayment(rs.getString("rec_payment"));
            reciept.setEmployeeId(rs.getInt("emp_id"));
            reciept.setCustomerId(rs.getInt("cus_id"));
            // Population
            CustomerDao customerDao = new CustomerDao();
            EmployeeDao empDao = new EmployeeDao();
            Customer customer = customerDao.get(reciept.getCustomerId());
            Employee emp = empDao.get(reciept.getEmployeeId());
            reciept.setCustomer(customer);
            reciept.setEmployee(emp);
        } catch (SQLException ex) {
            Logger.getLogger(Reciept.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return reciept;
    }

    
    
    
    
    
    
    
    
}
