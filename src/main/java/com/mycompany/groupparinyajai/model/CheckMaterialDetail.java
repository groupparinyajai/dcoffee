/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.groupparinyajai.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author chana
 */
public class CheckMaterialDetail {

    private int id;
    private String name;
    private int qtyLast;
    private int qtyRemain;
    private int materialId;
    private int checkMaterialId;

    public CheckMaterialDetail(int id, String name, int lastQty, int qtyRemain, int materialId, int checkMaterialId) {
        this.id = id;
        this.name = name;
        this.qtyLast = lastQty;
        this.qtyRemain = qtyRemain;
        this.materialId = materialId;
        this.checkMaterialId = checkMaterialId;
    }

    public CheckMaterialDetail(String name, int lastQty, int qtyRemain, int materialId, int checkMaterialId) {
        this.id = -1;
        this.name = name;
        this.qtyLast = lastQty;
        this.qtyRemain = qtyRemain;
        this.materialId = materialId;
        this.checkMaterialId = checkMaterialId;
    }

    public CheckMaterialDetail() {
        this.id = -1;
        this.name = "";
        this.qtyLast = 0;
        this.qtyRemain = 0;
        this.materialId = 0;
        this.checkMaterialId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQtyLast() {
        return qtyLast;
    }

    public void setQtyLast(int qtyLast) {
        this.qtyLast = qtyLast;
    }

    public int getQtyRemain() {
        return qtyRemain;
    }

    public void setQtyRemain(int qtyRemain) {
        this.qtyRemain = qtyRemain;
    }

    public int getMaterialId() {
        return materialId;
    }

    public void setMaterialId(int materialId) {
        this.materialId = materialId;
    }

    public int getCheckMaterialId() {
        return checkMaterialId;
    }

    public void setCheckMaterialId(int checkMaterialId) {
        this.checkMaterialId = checkMaterialId;
    }

    @Override
    public String toString() {
        return "CheckMaterialDetail{" + "id=" + id + ", name=" + name + ", qtyLast=" + qtyLast + ", qtyRemain=" + qtyRemain + ", materialId=" + materialId + ", checkMaterialId=" + checkMaterialId + '}';
    }

    public static CheckMaterialDetail fromRS(ResultSet rs) {
        CheckMaterialDetail checkMaterialDetail = new CheckMaterialDetail();
        try {
            checkMaterialDetail.setId(rs.getInt("cmd_id"));
            checkMaterialDetail.setName(rs.getString("cmd_name"));
            checkMaterialDetail.setQtyLast(rs.getInt("cmd_qty_last"));
            checkMaterialDetail.setQtyRemain(rs.getInt("cmd_qty_remain"));
            checkMaterialDetail.setMaterialId(rs.getInt("mat_id"));
            checkMaterialDetail.setCheckMaterialId(rs.getInt("check_mat_id"));
        } catch (SQLException ex) {
            Logger.getLogger(CheckMaterialDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkMaterialDetail;

    }
}
