/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.groupparinyajai.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kitta
 */
public class Bill {
    private int id;
    private String shopName;
    private Date date;
    private float total;
    private float buy;
    private float change;
    private int employeeId;

    public Bill(int id, String shopName, Date date, float total, float buy, float change, int employeeId) {
        this.id = id;
        this.shopName = shopName;
        this.date = date;
        this.total = total;
        this.buy = buy;
        this.change = change;
        this.employeeId = employeeId;
    }
    
    public Bill(String shopName, Date date, float total, float buy, float change, int employeeId) {
        this.id = -1;
        this.shopName = shopName;
        this.date = date;
        this.total = total;
        this.buy = buy;
        this.change = change;
        this.employeeId = employeeId;
    }
    
    public Bill() {
        this.id = -1;
        this.shopName = "";
        this.date = null;
        this.total = 0;
        this.buy = 0;
        this.change = 0;
        this.employeeId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getBuy() {
        return buy;
    }

    public void setBuy(float buy) {
        this.buy = buy;
    }

    public float getChange() {
        return change;
    }

    public void setChange(float change) {
        this.change = change;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public String toString() {
        return "Bill{" + "id=" + id + ", shopName=" + shopName + ", date=" + date + ", total=" + total + ", buy=" + buy + ", change=" + change + ", employeeId=" + employeeId + '}';
    }
    
    

 

    public static Bill fromRS(ResultSet rs) {
        Bill bill = new Bill();
        try {
            bill.setId(rs.getInt("bill_id"));
            bill.setShopName(rs.getString("bill_shop_name"));
            bill.setDate(rs.getTimestamp("bill_date"));
            bill.setTotal(rs.getFloat("bill_total"));
            bill.setBuy(rs.getFloat("bill_buy"));
            bill.setChange(rs.getFloat("bill_change"));
            bill.setChange(rs.getInt("emp_id"));
        } catch (SQLException ex) {
            Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return bill;
    }
    
    
}
