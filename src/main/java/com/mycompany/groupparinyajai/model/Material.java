/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.groupparinyajai.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tinti
 */
public class Material {
    private int id;
    private String name;
    private int minQty;
    private int Qty;
    private String unit;
    private float pricePerUnit;

    public Material(int id, String name, int minQty, int Qty, String unit, float pricePerUnit) {
        this.id = id;
        this.name = name;
        this.minQty = minQty;
        this.Qty = Qty;
        this.unit = unit;
        this.pricePerUnit = pricePerUnit;
    }

    public Material(String name, int minQty, int Qty, String unit, float pricePerUnit) {
        this.id = -1;
        this.name = name;
        this.minQty = minQty;
        this.Qty = Qty;
        this.unit = unit;
        this.pricePerUnit = pricePerUnit;
    }

    public Material() {
        this.id = -1;
        this.name = "";
        this.minQty = 0;
        this.Qty = 0;
        this.unit = "";
        this.pricePerUnit = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMinQty() {
        return minQty;
    }

    public void setMinQty(int minQty) {
        this.minQty = minQty;
    }

    public int getQty() {
        return Qty;
    }

    public void setQty(int Qty) {
        this.Qty = Qty;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public float getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(float pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    @Override
    public String toString() {
        return "material{" + "id=" + id + ", name=" + name + ", minQty=" + minQty + ", Qty=" + Qty + ", unit=" + unit + ", pricePerUnit=" + pricePerUnit + '}';
    }
    
    public static Material fromRS(ResultSet rs) {
        Material material = new Material();
        try {
            material.setId(rs.getInt("mat_id"));
            material.setName(rs.getString("mat_name"));
            material.setMinQty(rs.getInt("mat_min_qty"));
            material.setQty(rs.getInt("mat_qty"));
            material.setUnit(rs.getString("mat_unit"));
            material.setPricePerUnit(rs.getFloat("mat_price_per_unit"));
        } catch (SQLException ex) {
            Logger.getLogger(Material.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return material;
    }
    
    public boolean isValid(){
        return this.name.length()>=0 &&
                this.minQty>=1 &&
                this.Qty>=this.minQty&&
                this.unit.length()>=0 &&
                this.pricePerUnit>=0;
    }
//    public boolean isValidCheck(){
//        return this.name.length()>=0 &&
//                this.minQty>=1 &&
//                this.unit.length()>=0 &&
//                this.pricePerUnit>=0;
//    }
    
    
}
