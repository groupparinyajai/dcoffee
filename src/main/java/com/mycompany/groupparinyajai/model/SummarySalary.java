/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.groupparinyajai.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tinti
 */
public class SummarySalary {
    private int id;
    private Date salaryDate;
    private String workHour;
    private float Salary;
    private int empId;

    public SummarySalary(int id, Date salaryDate, String workHour, float Salary, int empId) {
        this.id = id;
        this.salaryDate = salaryDate;
        this.workHour = workHour;
        this.Salary = Salary;
        this.empId = empId;
    }
    
       public SummarySalary( Date salaryDate, String workHour, float Salary, int empId) {
        this.id = 0;
        this.salaryDate = salaryDate;
        this.workHour = workHour;
        this.Salary = Salary;
        this.empId = empId;
    }
       
    public SummarySalary() {
        this.id = 0;
        this.salaryDate = null;
        this.workHour = null;
        this.Salary = 0;
        this.empId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getSalaryDate() {
        return salaryDate;
    }

    public void setSalaryDate(Date salaryDate) {
        this.salaryDate = salaryDate;
    }

    public String getWorkHour() {
        return workHour;
    }

    public void setWorkHour(String workHour) {
        this.workHour = workHour;
    }

    public float getSalary() {
        return Salary;
    }

    public void setSalary(float Salary) {
        this.Salary = Salary;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    @Override
    public String toString() {
        return "SummarySalary{" + "id=" + id + ", salaryDate=" + salaryDate + ", workHour=" + workHour + ", Salary=" + Salary + ", empId=" + empId + '}';
    }
       
    public static SummarySalary fromRS(ResultSet rs) {
//       SummarySalary summarySalary = new SummarySalary();
          SummarySalary summarySalary = new SummarySalary();
        try {
            summarySalary.setId(rs.getInt("ss_id"));
            summarySalary. setSalaryDate(rs.getTimestamp("ss_date"));
            summarySalary.setWorkHour(rs.getString("ss_work_hour"));
            summarySalary.setSalary(rs.getFloat("ss_salary"));
           summarySalary.setEmpId(rs.getInt("emp_id"));
        } catch (SQLException ex) {
            Logger.getLogger(RecieptDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return summarySalary;
    }
    
}
