/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.groupparinyajai.model;

import com.mycompany.groupparinyajai.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.time.Duration;
import java.time.LocalTime;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mookda
 */
public class CheckInOut {

    private int id;
    private String date;
    private String timeIn;
    private String timeOut;
    private Float totalHour;
    private int userId;
    private int salaryId;
    private User user;

    public CheckInOut(int id, String date, String timeIn, String timeOut, Float totalHour, int userId, int salaryId) {
        this.id = id;
        this.date = date;
        this.timeIn = timeIn;
        this.timeOut = timeOut;
        this.totalHour = totalHour;
        this.userId = userId;
        this.salaryId = salaryId;
    }

    public CheckInOut(String date, String timeIn, String timeOut, Float totalHour, int userId, int salaryId) {
        this.id = -1;
        this.date = date;
        this.timeIn = timeIn;
        this.timeOut = timeOut;
        this.totalHour = totalHour;
        this.userId = userId;
        this.salaryId = salaryId;
    }

    public CheckInOut() {
        this.id = -1;
        this.date = null;
        this.timeIn = null;
        this.timeOut = "";
        this.totalHour = 0.00f;
        this.userId = 0;
        this.salaryId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(String timeIn) {
        this.timeIn = timeIn;
    }

    public String getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }

    public Float getTotalHour() {
        return totalHour;
    }

    public void setTotalHour(Float totalHour) {
        this.totalHour = totalHour;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getSalaryId() {
        return salaryId;
    }

    public void setSalaryId(int salaryId) {
        this.salaryId = salaryId;
    }
    
    

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "CheckInOut{" + "id=" + id + ", date=" + date + ", timeIn=" + timeIn + ", timeOut=" + timeOut + ", totalHour=" + totalHour + ", userId=" + userId + ", salaryId=" + salaryId + '}';
    }

    public static CheckInOut fromRS(ResultSet rs) {
        CheckInOut checkInOut = new CheckInOut();
        try {
            checkInOut.setId(rs.getInt("cio_id"));
            checkInOut.setDate(rs.getString("cio_date"));
            checkInOut.setTimeIn(rs.getString("cio_time_in"));
            checkInOut.setTimeOut(rs.getString("cio_time_out"));
            checkInOut.setTotalHour(rs.getFloat("cio_total_hour"));
            checkInOut.setUserId(rs.getInt("emp_id"));
            checkInOut.setSalaryId(rs.getInt("ss_id"));

            UserDao userDao = new UserDao();
            User user = userDao.get(checkInOut.getUserId());
            checkInOut.setUser(user);

        } catch (SQLException ex) {
            Logger.getLogger(CheckInOut.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkInOut;
    }

}
