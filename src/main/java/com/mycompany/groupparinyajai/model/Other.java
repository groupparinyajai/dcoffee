/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.groupparinyajai.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author win10
 */
public class Other {

    private int id;
    private String name;
    private int qty;
    private Date date;
    private float pricePerunit;
    private float total;

    public Other(int id, String name, int qty, Date date, float pricePerunit, float total) {
        this.id = id;
        this.name = name;
        this.qty = qty;
        this.date = date;
        this.pricePerunit = pricePerunit;
        this.total = total;

    }

    public Other(String name, int qty, Date date, float pricePerunit, float total) {
        this.id = -1;
        this.name = name;
        this.qty = qty;
        this.date = date;
        this.pricePerunit = pricePerunit;
        this.total = total;

    }

    public Other() {
        this.id = -1;
        this.name = "";
        this.qty = 0;
        this.date = null;
        this.pricePerunit = 0;
        this.total = 0;
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public float getPricePerunit() {
        return pricePerunit;
    }

    public void setPricePerunit(float pricePerunit) {
        this.pricePerunit = pricePerunit;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Other{" + "id=" + id + ", name=" + name + ", qty=" + qty + ", date=" + date + ", pricePerunit=" + pricePerunit + ", total=" + total + '}';
    }

    public static Other fromRS(ResultSet rs) {
        Other other = new Other();
        try {
            other.setId(rs.getInt("oth_id"));
            other.setName(rs.getString("oth_name"));
            other.setQty(rs.getInt("oth_qty"));
            other.setDate(rs.getDate("oth_date"));
            other.setPricePerunit(rs.getFloat("oth_price_per_unit"));
            other.setTotal(rs.getFloat("oth_total"));
        } catch (SQLException ex) {
            Logger.getLogger(Other.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return other;
    }

}
