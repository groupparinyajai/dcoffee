/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.groupparinyajai.service;

import com.mycompany.groupparinyajai.dao.CategoryDao;
import com.mycompany.groupparinyajai.model.Category;
import java.util.List;

/**
 *
 * @author tinti
 */
public class CategoryService {
    public Category getById (int id) {
        CategoryDao categoryDao = new CategoryDao();
        Category category = categoryDao.getCategoryById(id);

        return category;
    }

    public List<Category> getCategory() {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.getAll("category_id asc");
    }

    public Category addNew(Category editedCategory) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.save(editedCategory);
    }

    public Category update(Category editedCategory) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.update(editedCategory);
    }

    public int delete(Category editedCategory) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.delete(editedCategory);
    } 
}
