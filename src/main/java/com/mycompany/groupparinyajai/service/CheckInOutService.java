/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.groupparinyajai.service;

import com.mycompany.groupparinyajai.dao.CheckInOutDao;
import com.mycompany.groupparinyajai.model.CheckInOut;
import java.util.List;

/**
 *
 * @author mookda
 */
public class CheckInOutService {

    public CheckInOut getById(int id) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.get(id);
    }

    public List<CheckInOut> getCheckInOuts() {
        CheckInOutDao CheckInOutDao = new CheckInOutDao();
        return CheckInOutDao.getAll(" cio_id asc");
    }

    public List<CheckInOut> getCheckInOutsByUserId(int userId) {
        CheckInOutDao CheckInOutDao = new CheckInOutDao();
        return CheckInOutDao.getAllByUserId(userId);
    }

    public CheckInOut addNew(CheckInOut editedCheckInOut) {
        CheckInOutDao CheckInOutDao = new CheckInOutDao();
        return CheckInOutDao.save(editedCheckInOut);
    }

    public CheckInOut update(CheckInOut editedCheckInOut) {
        CheckInOutDao CheckInOutDao = new CheckInOutDao();
        return CheckInOutDao.update(editedCheckInOut);
    }

    public int delete(CheckInOut editedCheckInOut) {
        CheckInOutDao CheckInOutDao = new CheckInOutDao();
        return CheckInOutDao.delete(editedCheckInOut);
    }
}
