/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.groupparinyajai.service;

import com.mycompany.groupparinyajai.dao.BillDao;
import com.mycompany.groupparinyajai.model.Bill;
import java.util.List;

/**
 *
 * @author chana
 */
public class BillService {

    public Bill getById (int id) {
        BillDao billDao = new BillDao();
        Bill bill = billDao.getById(id);

        return bill;
    }

    public List<Bill> getBills() {
        BillDao billDao = new BillDao();
        return billDao.getAll(" bill_id asc");
    }

    public Bill addNew(Bill editedBill) {
        BillDao billDao = new BillDao();
        return billDao.save(editedBill);
    }

    public Bill update(Bill editedBill) {
        BillDao billDao = new BillDao();
        return billDao.update(editedBill);
    }

    public int delete(Bill editedBill) {
        BillDao billDao = new BillDao();
        return billDao.delete(editedBill);
    }
}
