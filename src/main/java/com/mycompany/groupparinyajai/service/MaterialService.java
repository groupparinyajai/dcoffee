/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.groupparinyajai.service;

import ValidateException.ValidateException;
import com.mycompany.groupparinyajai.dao.MaterialDao;
import com.mycompany.groupparinyajai.model.Material;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Asus
 */
public class MaterialService {

    public ArrayList<Material> getMaterialsOrderbyName() {
        MaterialDao materialDao = new MaterialDao();
        return (ArrayList<Material>) materialDao.getAll(" mat_name ASC");
    }

    public List<Material> getMaterials() {
        MaterialDao materialDao = new MaterialDao();
        return (ArrayList<Material>) materialDao.getAll(" mat_qty asc");
    }

    public Material getId(int id) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.get(id);
    }

    public Material addNew(Material editedMaterial) throws ValidateException {
        if (!editedMaterial.isValid()) {
            throw new ValidateException("material is invalid!!!");
        }
        MaterialDao materialDao = new MaterialDao();
        return materialDao.save(editedMaterial);
    }

    public Material update(Material editedMaterial) throws ValidateException {
        if (!editedMaterial.isValid()) {
            throw new ValidateException("material is invalid!!!");
        }
        MaterialDao materialDao = new MaterialDao();
        return materialDao.update(editedMaterial);
    }

    public int delete(Material editedMaterial) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.delete(editedMaterial);
    }
}
