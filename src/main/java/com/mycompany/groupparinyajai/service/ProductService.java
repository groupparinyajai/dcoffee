/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.groupparinyajai.service;

import com.mycompany.groupparinyajai.dao.ProductDao;

import com.mycompany.groupparinyajai.model.Product;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author chana
 */
public class ProductService {

    public List<Product> getProducts() {
        ProductDao productDao = new ProductDao();
        return productDao.getAll(" product_id asc");
    }

    public ArrayList<Product> getProductsOrderByName() {
        ProductDao productDao = new ProductDao();
        return (ArrayList<Product>) productDao.getAll(" product_name ASC ");
    }

    public Product addNew(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.save(editedProduct);
    }

    public Product update(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.update(editedProduct);
    }

    public int delete(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.delete(editedProduct);
    }
}
