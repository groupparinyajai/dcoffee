/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.groupparinyajai.service;

import com.mycompany.groupparinyajai.dao.SummarySalaryDao;
import com.mycompany.groupparinyajai.model.SummarySalary;
import java.util.List;

/**
 *
 * @author tinti
 */
public class SummarySalaryService {
      public SummarySalary getById(int id) {
        SummarySalaryDao salaryDao = new SummarySalaryDao();
        return salaryDao.get(id);
    }

    public List<SummarySalary> getSummarySalarys() {
        SummarySalaryDao SummarySalaryDao = new SummarySalaryDao();
        return SummarySalaryDao.getAll(" ss_id asc");
    }

    public SummarySalary addNew(SummarySalary editedSummarySalary) {
        SummarySalaryDao SummarySalaryDao = new SummarySalaryDao();
        return SummarySalaryDao.save(editedSummarySalary);
    }

    public SummarySalary update(SummarySalary editedSummarySalary) {
        SummarySalaryDao SummarySalaryDao = new SummarySalaryDao();
        return SummarySalaryDao.update(editedSummarySalary);
    }

    public int delete(SummarySalary editedSummarySalary) {
        SummarySalaryDao SummarySalaryDao = new SummarySalaryDao();
        return SummarySalaryDao.delete(editedSummarySalary);
    }
}
