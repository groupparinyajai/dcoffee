/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.groupparinyajai.service;

import com.mycompany.groupparinyajai.dao.EmployeeDao;
import com.mycompany.groupparinyajai.model.Employee;
import java.util.List;

/**
 *
 * @author chana
 */
public class EmployeeService {

    public Employee getByName(String name) {
        EmployeeDao employeeDao = new EmployeeDao();
        Employee employee = employeeDao.getByTel(name);

        return employee;
    }

    public List<Employee> getEmployees() {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.getAll(" emp_id asc");
    }

    public Employee addNew(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.save(editedEmployee);
    }

    public Employee update(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.update(editedEmployee);
    }

    public int delete(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.delete(editedEmployee);
    }
}
