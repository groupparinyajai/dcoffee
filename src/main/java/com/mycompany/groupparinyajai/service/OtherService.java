/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.groupparinyajai.service;

import com.mycompany.groupparinyajai.dao.OtherDao;
import com.mycompany.groupparinyajai.model.Other;
import java.util.List;

/**
 *
 * @author win10
 */
public class OtherService {

    public Other getById (int id) {
        OtherDao otherDao = new OtherDao();
        Other other = otherDao.getById(id);

        return other;
    }

    public List<Other> getOthers() {
        OtherDao otherDao = new OtherDao();
        return otherDao.getAll(" oth_id asc");
    }

    public Other addNew(Other editedOther) {
        OtherDao otherDao = new OtherDao();
        return otherDao.save(editedOther);
    }

    public Other update(Other editedOther) {
        OtherDao otherDao = new OtherDao();
        return otherDao.update(editedOther);
    }

    public int delete(Other editedOther) {
        OtherDao otherDao = new OtherDao();
        return otherDao.delete(editedOther);
    }
    
   
}
