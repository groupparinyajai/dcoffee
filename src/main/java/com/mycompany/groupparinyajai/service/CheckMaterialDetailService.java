/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.groupparinyajai.service;

import com.mycompany.groupparinyajai.dao.CheckMaterialDetailDao;
import com.mycompany.groupparinyajai.model.CheckMaterialDetail;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Asus
 */
public class CheckMaterialDetailService {
    private final CheckMaterialDetailDao cmdDao = new CheckMaterialDetailDao();
    public ArrayList<CheckMaterialDetail> getCheckMaterialDetailOrderbyId(){
        return (ArrayList<CheckMaterialDetail>) cmdDao.getAll(" cmd_id ASC");
    }
    
    public List<CheckMaterialDetail> getCheckMaterialDetail(){
        CheckMaterialDetailDao cmdDao = new CheckMaterialDetailDao();
        return cmdDao.getAll(" mat_id asc");
    }

    public CheckMaterialDetail addNew(CheckMaterialDetail editedCheckMaterialDetail) {
        CheckMaterialDetailDao cmdDao = new CheckMaterialDetailDao();
        return cmdDao.save(editedCheckMaterialDetail);
    }

    public CheckMaterialDetail update(CheckMaterialDetail editedCheckMaterialDetail) {
        CheckMaterialDetailDao cmdDao = new CheckMaterialDetailDao();
        return cmdDao.update(editedCheckMaterialDetail);
    }

    public int delete(CheckMaterialDetail editedCheckMaterialDetail) {
        CheckMaterialDetailDao cmdDao = new CheckMaterialDetailDao();
        return cmdDao.delete(editedCheckMaterialDetail);
    }
   
}
