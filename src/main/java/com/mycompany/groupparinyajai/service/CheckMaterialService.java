/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.groupparinyajai.service;

import com.mycompany.groupparinyajai.dao.CheckMaterialDao;
import com.mycompany.groupparinyajai.dao.CheckMaterialDetailDao;
import com.mycompany.groupparinyajai.model.CheckMaterial;
import com.mycompany.groupparinyajai.model.CheckMaterialDetail;
import com.mycompany.groupparinyajai.model.Material;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class CheckMaterialService {
    private final CheckMaterialDao checkMaterialDao = new CheckMaterialDao();
    public ArrayList<CheckMaterial> getCheckMaterialsOrderbyId(){
        return (ArrayList<CheckMaterial>) checkMaterialDao.getAll(" check_mat_id ASC");
    }
    
//    public List<CheckMaterial> getCheckMaterials(){
//        CheckMaterialDao checkMaterialDao = new CheckMaterialDao();
//        return checkMaterialDao.getAll(" mat_id asc");
//    }

    public CheckMaterial addNew(CheckMaterial editedCheckMaterial) {
        MaterialService matService = new MaterialService();
        CheckMaterialDao checkMaterialDao = new CheckMaterialDao();
        CheckMaterialDetailDao cmdDao = new CheckMaterialDetailDao();
        CheckMaterial checkMat = checkMaterialDao.save(editedCheckMaterial);
        for(CheckMaterialDetail cmd : editedCheckMaterial.getCmd()){
            try{
                Material mat = matService.getId(cmd.getMaterialId());
                mat.setQty(cmd.getQtyRemain());
                matService.update(mat);
                cmd.setCheckMaterialId(checkMat.getId());
                cmdDao.save(cmd);
            } catch (Exception ex) {
                Logger.getLogger(CheckMaterialService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return checkMat;
    }

    public CheckMaterial update(CheckMaterial editedCheckMaterial) {
        CheckMaterialDao checkMaterialDao = new CheckMaterialDao();
        return checkMaterialDao.update(editedCheckMaterial);
    }

    public int delete(CheckMaterial editedCheckMaterial) {
        CheckMaterialDao checkMaterialDao = new CheckMaterialDao();
        return checkMaterialDao.delete(editedCheckMaterial);
    }

}
