/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.groupparinyajai.service;

import com.mycompany.groupparinyajai.dao.BestSellerDao;
import com.mycompany.groupparinyajai.model.BestSeller;
import java.util.List;

/**
 *
 * @author win10
 */
public class BestSellerService {
    public List<BestSeller> getTopTenProductByTotalPrice(){
        BestSellerDao bestsellerDao = new BestSellerDao();
        return bestsellerDao.getProductByTotalPrice(10);
    }
      public List<BestSeller> getTopTenProductByTotalPrice(String begin,String end){
        BestSellerDao bestsellerDao = new BestSellerDao();
        return bestsellerDao.getProductByTotalPrice(begin,end,10);
    }
}
