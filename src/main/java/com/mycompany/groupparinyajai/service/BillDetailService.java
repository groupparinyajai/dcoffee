/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.groupparinyajai.service;

import com.mycompany.groupparinyajai.dao.BillDetailDao;
import com.mycompany.groupparinyajai.model.BillDetail;
import java.util.List;

/**
 *
 * @author chana
 */
public class BillDetailService {

    public BillDetail getById (int id) {
        BillDetailDao billDetailDao = new BillDetailDao();
        BillDetail billDetail = billDetailDao.getById(id);

        return billDetail;
    }

    public List<BillDetail> getBillDetails() {
        BillDetailDao billDetailDao = new BillDetailDao();
        return billDetailDao.getAll(" bill_detail_id asc");
    }

    public BillDetail addNew(BillDetail editedBillDetail) {
        BillDetailDao billDetailDao = new BillDetailDao();
        return billDetailDao.save(editedBillDetail);
    }

    public BillDetail update(BillDetail editedBillDetail) {
        BillDetailDao billDetailDao = new BillDetailDao();
        return billDetailDao.update(editedBillDetail);
    }

    public int delete(BillDetail editedBillDetail) {
        BillDetailDao billDetailDao = new BillDetailDao();
        return billDetailDao.delete(editedBillDetail);
    }
}
