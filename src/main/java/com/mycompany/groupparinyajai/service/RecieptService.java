/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.groupparinyajai.service;

import com.mycompany.groupparinyajai.dao.RecieptDao;
import com.mycompany.groupparinyajai.dao.RecieptDetailDao;
import com.mycompany.groupparinyajai.model.Reciept;
import com.mycompany.groupparinyajai.model.RecieptDetail;
import java.util.List;

/**
 *
 * @author Asus
 */
public class RecieptService {
    public Reciept getById(int id) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.get(id);
        
    }
    public List<Reciept> getReciepts(){
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.getAll(" rec_id desc");
    }

    public Reciept addNew(Reciept editedReciept) {
        RecieptDao recieptDao = new RecieptDao();
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        Reciept reciept = recieptDao.save(editedReciept);
        for(RecieptDetail rd: editedReciept.getRecieptDetails()){
            rd.setRecieptId(reciept.getId());
            recieptDetailDao.save(rd);
        }
        return reciept;
    }

    public Reciept update(Reciept editedReciept) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.update(editedReciept);
    }

    public int delete(Reciept editedReciept) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.delete(editedReciept);
    }
}
