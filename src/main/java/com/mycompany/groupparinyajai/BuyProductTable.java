/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mycompany.groupparinyajai;

import com.mycompany.groupparinyajai.model.Product;

/**
 *
 * @author ASUS
 */
public interface BuyProductTable {
    public void buy(Product product, int qty);
}
